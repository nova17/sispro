<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpseclib\\' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Stonks\\DataLayer\\' => array($vendorDir . '/stonks/datalayer/src'),
    'Scripts\\' => array($baseDir . '/scripts'),
    'RouterOS\\' => array($vendorDir . '/evilfreelancer/routeros-api-php/src'),
    'Models\\' => array($baseDir . '/app/models'),
    'DivineOmega\\SSHConnection\\' => array($vendorDir . '/divineomega/php-ssh-connection/src'),
    'Controllers\\' => array($baseDir . '/app/controllers'),
    'CoffeeCode\\Router\\' => array($vendorDir . '/coffeecode/router/src'),
);
